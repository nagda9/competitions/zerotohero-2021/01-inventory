// Data Transfer Object for injecting inventory request body data
export class InventoryDto {
    readonly employeeId: string;
    readonly itemId: string;
    readonly epochSecond: number;
}
