import path = require("path");

// csv files are created and reached by these
export const db = path.join(__dirname, 'inventory.csv')
export const originalRequestsCsv = path.join(__dirname, 'original_requests.csv')