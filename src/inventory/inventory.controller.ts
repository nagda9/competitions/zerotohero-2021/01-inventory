import {Controller, Post, Get, Put, Body, Param} from '@nestjs/common';
import {InventoryService} from "./inventory.service";
import {InventoryDto} from "./dto/inventory.dto";
import {Inventory} from "./interface/inventory.interface";
import {Cache} from "cache-manager";

@Controller('inventory')
export class InventoryController {
    private readonly cacheManager : Cache
    constructor(private readonly InventoryService: InventoryService) {
        this.cacheManager = this.InventoryService.getCacheManager()
    }

    // InventoryService's cacheManager, used outside of provider and controller only for testing
    public getCacheManager = () => {
        return this.cacheManager;
    }

    // POST endpoint for /inventory/
    @Post()
    create(@Body() inventoryDto: InventoryDto) : Promise<string> {
        return this.InventoryService.create(inventoryDto)
    }

    // GET endpoint for /inventory/
    @Get()
    getAll() : Promise<Array<Inventory>> {
        return this.InventoryService.getAll()
    }

    // GET endpoint for /inventory/:id
    @Get(':id')
    getOne(@Param('id') id) : Promise<Inventory> {
        return this.InventoryService.getOne(id)
    }

    // PUT endpoint for /inventory/:id
    @Put(':id')
    update(@Body() inventoryDto: InventoryDto, @Param('id') id): Promise<void> {
        return this.InventoryService.update(inventoryDto, id)
    }
}
