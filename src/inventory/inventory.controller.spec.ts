import {Test, TestingModule} from '@nestjs/testing';
import {InventoryController} from './inventory.controller';
import {InventoryService} from "./inventory.service";
import moment = require("moment");
import {deleteCsv, generateOriginalRequestsCsv, parseInventoryCsv} from "./functions/handle-csv";
import {db} from "./csv/csv-paths";
import {Inventory} from "./interface/inventory.interface";
import {CacheModule} from "@nestjs/common";

// testing POST, GET and PUT requests (including caching)
describe('inventory Controller', () => {
    let controller: InventoryController;

    // mockup data for original_requests.csv
    const originalData : Array<string> =
        ["ITEM_ID;EMPLOYEE_ID",
            "F4356FV02;325VFFDG4",
            "ZE1C68MWH;NB8LW0SX4",
            "XHCJRPN8H;RK888O9IW"]

    // generate original_requests.csv if missing
    generateOriginalRequestsCsv(originalData.join("\n"));

    // empty the inventory.csv file
    deleteCsv(db)

    const ids : Array<string> = []

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [CacheModule.register()],
            controllers: [InventoryController],
            providers: [InventoryService]
        }).compile();

        controller = module.get<InventoryController>(InventoryController);
    });

    // Examine the inventory.csv file and check if the data with the correct attributes is there
    const checkParams : Function = async (employeeId, itemId, originalOwnerId, investigation, timestampUTC) => {
        const entities : Array<Inventory> = await parseInventoryCsv(controller.getCacheManager())
        expect(entities.find(entity =>
            entity.employeeId === employeeId
            && entity.itemId === itemId
            && entity.timestamp === timestampUTC
            && entity.investigation === investigation
            && entity.originalOwnerId === originalOwnerId
        )).toBeTruthy()
    }

    // Generate POST request based on parameters
    const createPOST : Function = async (employeeId, itemId, epochSecondUnix) => {
        const id = await controller.create({
            "employeeId": employeeId,
            "itemId": itemId,
            "epochSecond": epochSecondUnix
        })
        ids.push(id)
    }

    // Generate PUT request based on parameters
    const createPUT : Function = async (employeeId, itemId, epochSecondUnix, id) => {
        await controller.update({
            "employeeId": employeeId,
            "itemId": itemId,
            "epochSecond": epochSecondUnix
        }, id)
    }

    it('POST: item and employee both exist, employee is original owner', async () => {
        const itemId = "F4356FV02"
        const employeeId = "325VFFDG4"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        await checkParams(employeeId, itemId, "", "NOT REQUIRED", epochSecondUTC)
    });

    it('POST: item and employee both exist, employee is NOT original owner', async () => {
        const itemId = "F4356FV02"
        const employeeId = "NB8LW0SX4"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        await checkParams(employeeId, itemId, "325VFFDG4", "NOT REQUIRED", epochSecondUTC)
    });

    it('POST: item exist and employee missing', async () => {
        const itemId = "F4356FV02"
        const employeeId = "010101010"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        await checkParams(employeeId, itemId, "325VFFDG4", "REQUIRED", epochSecondUTC)
    });

    it('POST: item missing and employee exist', async () => {
        const itemId = "010101010"
        const employeeId = "325VFFDG4"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        checkParams(employeeId, itemId, "", "REQUIRED", epochSecondUTC)
    });

    it('POST: item and employee missing', async () => {
        const itemId = "010101010"
        const employeeId = "010101010"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        checkParams(employeeId, itemId, "", "REQUIRED", epochSecondUTC)
    });

    it('GET test: get elements one by one, should be equal with getting all', async () => {
        const entities = []
        for (const id of ids) {
            entities.push(await controller.getOne(id))
        }
        console.log(entities)
        expect(await controller.getAll()).toStrictEqual(entities)
    });

    it('PUT test: change missing item to existing item, existing employee, employee is original owner', async () => {
        const itemId = "010101010"
        const employeeId = "325VFFDG4"
        const epochSecondUnix = moment().unix();
        const epochSecondUTC = moment.unix(epochSecondUnix).utc().format()

        await createPOST(employeeId, itemId, epochSecondUnix)
        checkParams(employeeId, itemId, "", "REQUIRED", epochSecondUTC)

        const newItemId = "F4356FV02"

        await createPUT(employeeId, newItemId, epochSecondUnix, ids[ids.length-1])
        checkParams(employeeId, newItemId, "", "NOT REQUIRED", epochSecondUTC)
    });
});
