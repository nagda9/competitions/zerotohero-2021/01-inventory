import {db, originalRequestsCsv} from "../csv/csv-paths"
import {Inventory} from "../interface/inventory.interface";
import {OriginalRequest} from "../interface/original-request.interface";
import {Cache} from "cache-manager";

const fs = require('fs')
const parse = require('csv-parser')

// Generate inventory.csv file if missing, when the InventoryService is constructed
export const generateInventoryCsv = () => {
    const inventoryData = 'ID;EMPLOYEE_ID;ITEM_ID;ORIGINAL_OWNER_ID;INVESTIGATION;TIMESTAMP\n';
    try {
        if (!fs.existsSync(db)) {
            fs.writeFileSync(db, inventoryData)
            console.log("'inventory.csv' is created successfully.")
        }
    } catch(err) {
        console.log(err.message)
    }
}

// Generate original_requests.csv file if missing from mockup data, when the InventoryService is constructed
export const generateOriginalRequestsCsv = (originalData) => {
    try {
        if (!fs.existsSync(originalRequestsCsv)) {
            fs.writeFileSync(originalRequestsCsv, originalData)
            console.log("'original_requests.csv' is created successfully.")
        }
    } catch (err) {
        console.log(err.message)
    }
}

// Create an Inventory entity from a parsed Object with string keys
export const entityFromParsedData = (parsedEntity) => {
    const newEntity : Inventory = {
        id: parsedEntity["ID"],
        employeeId: parsedEntity["EMPLOYEE_ID"],
        itemId: parsedEntity["ITEM_ID"],
        originalOwnerId: parsedEntity["ORIGINAL_OWNER_ID"],
        investigation: parsedEntity["INVESTIGATION"],
        timestamp: parsedEntity["TIMESTAMP"]
    }
    return newEntity
}

// Parse all data from the inventory.csv file into an array of Inventories, add all data to cache
export const parseInventoryCsv = async (cacheManager: Cache) => {
    const entities : Array<Inventory> = []
    const stream = fs.createReadStream(db)
    const parser = stream.pipe(parse({separator:";"}))
    for await (const entity of parser) {
        const newEntity = entityFromParsedData(entity)
        entities.push(newEntity)
        if (cacheManager) {
            await cacheManager.set(newEntity.id, newEntity)
        }
    }
    return entities
}

// Parse all data from the original_requests.csv file into an array of OriginalRequests
export const parseOriginalRequestsCsv = async () => {
    const entities : Array<OriginalRequest> = []
    const stream = fs.createReadStream(originalRequestsCsv)
    const parser = stream.pipe(parse({separator:";"}))
    for await (const entity of parser) {
        const newEntity : OriginalRequest = {
            employeeId: entity["EMPLOYEE_ID"],
            itemId: entity["ITEM_ID"],
        }
        entities.push(newEntity)
    }
    return entities
}

// Delete temporary files (delete inventory.csv.new file after created by PUT request)
export const deleteCsv = (pathToCsv) => {
    if (fs.existsSync(pathToCsv)) {
        fs.unlinkSync(pathToCsv)
    }
}