// Generate alphabet for the inventory ID-s (regex: ^[A-Z0-9]{9}$)
export const generateAlphabet = () => {
    let alphabet = ""
    for (let i = 0; i < 36; i++ ) {
        alphabet += (i).toString(36).toUpperCase()
    }
    return alphabet
}
