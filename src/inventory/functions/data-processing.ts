import {InventoryDto} from "../dto/inventory.dto";
import {parseOriginalRequestsCsv} from "./handle-csv";
import {Inventory} from "../interface/inventory.interface";
import {OriginalRequest} from "../interface/original-request.interface";

const moment = require("moment")

// ORIGINAL_OWNER_ID and INVESTIGATION data decision based on original_requests.csv
export const processData = async (inventoryDto: InventoryDto, id: string) => {
    const originalRequests : Array<OriginalRequest> = await parseOriginalRequestsCsv()
    const entityOfItem : OriginalRequest = await originalRequests.find(originalReqEntity => originalReqEntity.itemId === inventoryDto.itemId)
    const entityOfEmployee : OriginalRequest = await originalRequests.find(originalReqEntity => originalReqEntity.employeeId === inventoryDto.employeeId)

    let originalOwner = ""
    let investigation = "NOT REQUIRED"

    // case 1: both employee and item exist
    if (entityOfItem && entityOfEmployee) {
        if (entityOfItem.employeeId !== inventoryDto.employeeId) { // not the original owner
            originalOwner = entityOfItem.employeeId
        }
    } else { // case 2,3,4: employee or item is missing
        // case 2: item exists, employee missing => not original owner exists
        if (entityOfItem && !entityOfEmployee) {
            originalOwner = entityOfItem.employeeId
        }
        // case 3: employee exists, item missing => no item, no original owner
        // case 4: employee and item missing => no item, no original owner
        investigation = "REQUIRED" // in all cases
    }

    const epochSecondsUTC = moment.unix(inventoryDto.epochSecond).utc().format()
    const newEntity : Inventory = {
        id: id,
        employeeId: inventoryDto.employeeId,
        itemId: inventoryDto.itemId,
        originalOwnerId: originalOwner,
        investigation: investigation,
        timestamp: epochSecondsUTC,
    }
    return newEntity
}