import {CACHE_MANAGER, Inject, Injectable} from '@nestjs/common'
import {Cache} from "cache-manager"
import {InventoryDto} from "./dto/inventory.dto"
import {generateAlphabet} from "./functions/generate-alphabet";
import {
    entityFromParsedData,
    generateInventoryCsv,
    generateOriginalRequestsCsv,
    parseInventoryCsv
} from "./functions/handle-csv";
import {processData} from "./functions/data-processing";
import {customAlphabet} from "nanoid";
import {db} from "./csv/csv-paths";
import {Inventory} from "./interface/inventory.interface";

const fs = require('fs')
const parse = require('csv-parser')

@Injectable()
export class InventoryService {
    private readonly alphabet: string
    private readonly nanoid: Function
    private testOriginalData : Array<string> =
        ["ITEM_ID;EMPLOYEE_ID",
            "F4356FV02;325VFFDG4",
            "ZE1C68MWH;NB8LW0SX4",
            "XHCJRPN8H;RK888O9IW"]

    constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {
        this.alphabet = generateAlphabet()
        this.nanoid = customAlphabet(this.alphabet, 9)
        generateInventoryCsv()
        generateOriginalRequestsCsv(this.testOriginalData.join("\n"))
    }

    // Getter for private cacheManager
    public getCacheManager = () => {
        return this.cacheManager;
    }

    // Create and insert processed data into the inventory.csv file, add data to cache
    async create(inventoryDto: InventoryDto){
        const id = this.nanoid()
        const newEntity = await processData(inventoryDto, id)

        await this.cacheManager.set(id, newEntity)

        try {
            fs.appendFileSync(db, Object.values(newEntity).join(";") + "\n")
            return id
        } catch (err) {
            return err.message
        }
    }

    // Get all rows/entities of the inventory.csv file, update cache
    async getAll() {
        return await parseInventoryCsv(this.cacheManager)
    }

    // Get a single entity by id, use cache if possible
    async getOne(id: string) {
        const entity : Inventory = await this.cacheManager.get(id)
        if (entity) {
            console.log("cache used")
            return entity
        } else {
            const entities : Array<Inventory> = await parseInventoryCsv(this.cacheManager)
            return entities.find(entity => entity.id === id)
        }
    }

    // Update an entity in the inventory.csv file, selected by id, update cache
    async update(inventoryDto: InventoryDto, id: string){
        const readStream = fs.createReadStream(db)
        const writeStream = fs.createWriteStream(`${db}.new`, {flags:'a'});
        writeStream.write('ID;EMPLOYEE_ID;ITEM_ID;ORIGINAL_OWNER_ID;INVESTIGATION;TIMESTAMP\n')

        const parser = readStream.pipe(parse({separator: ";"}))
        for await (const entity of parser) {
            const newEntity = entityFromParsedData(entity)
            if (newEntity.id === id) {
                const modifiedEntity = await processData(inventoryDto, id)
                await this.cacheManager.set(modifiedEntity.id, modifiedEntity)
                writeStream.write(Object.values(modifiedEntity).join(";") + "\n")
            } else {
                await this.cacheManager.set(newEntity.id, newEntity)
                writeStream.write(Object.values(entity).join(";") + "\n")
            }
        }
        writeStream.end()

        try {
            fs.renameSync(db, `${db}.old`)
            fs.renameSync(`${db}.new`, `${db}`)
        } catch (err) {
            console.log(err)
        }
    }
}
