export interface Inventory{
    id: string
    employeeId: string
    itemId: string
    originalOwnerId: string
    investigation: string
    timestamp: Date
}

