import {Module, CacheModule} from '@nestjs/common';
import {AppController} from "./app.controller";
import {AppService} from "./app.service";
import {InventoryController} from './inventory/inventory.controller';
import {InventoryService} from './inventory/inventory.service';

@Module({
    imports: [CacheModule.register()],
    controllers: [AppController, InventoryController],
    providers: [AppService, InventoryService],
})

export class AppModule {
}
